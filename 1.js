const eyeIcons = document.querySelectorAll(".icon-password");
const passwordInputs = document.querySelectorAll('input[type="password"]');


eyeIcons.forEach(function (eyeIcon, index) {
    eyeIcon.addEventListener("click", function () {
      if (passwordInputs[index].type === "password") {
        passwordInputs[index].type = "text";
      } else {
        passwordInputs[index].type = "password";
      }
      //функція яка змінює тип даних вікна input з password на text. Фактично дає змогу бачити символи введеного пароля.
      eyeIcon.classList.toggle("fa-eye");
      eyeIcon.classList.toggle("fa-eye-slash");
      // автоматична зміна классу при натисканні(зміна іконок)
    });
  });

  const submitBtn = document.querySelector('.submit-btn');
 submitBtn.addEventListener('click', function (event) {
  event.preventDefault();
  //Зупинка автоматичного відправлення форми, яке зробить перезавантаження сторінки.
    
    
  const passwordInput = document.querySelector('input[data-input="password"]');
  const confirmPasswordInput = document.querySelector('input[data-input="confirm-password"]');
  const errorMessage = document.querySelector('#error-message');
  //У цих рядках виконується пошук елементів <input> використовуючи селектори з атрибутами data-input.
    const password = passwordInput.value;
    const confirmPassword = confirmPasswordInput.value;
  //Присвоїти значення введенних паролів з двох строк введення для подальшого порівняння
    if (password === confirmPassword) {
        errorMessage.textContent = '';
        alert("You are welcome")
      
    } else {
        errorMessage.textContent = 'Потрібно ввести однакові значення'; // Встановити повідомлення про помилку
        errorMessage.style.display = 'block'; 
    }
  });
